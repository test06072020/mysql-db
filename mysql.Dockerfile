FROM mysql/mysql-server:5.7.30

RUN mkdir -p /etc/certs
COPY certs /etc/certs
RUN ls -al /etc/certs
COPY my.cnf /etc/
RUN chmod 0444 /etc/my.cnf

VOLUME /var/lib/mysql

EXPOSE 3306 33060
CMD ["mysqld"]
